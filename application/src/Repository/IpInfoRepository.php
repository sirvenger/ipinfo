<?php

namespace App\Repository;

use App\Entity\IpInfo;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Common\Persistence\ManagerRegistry;

/**
 * @method IpInfo|null find($id, $lockMode = null, $lockVersion = null)
 * @method IpInfo|null findOneBy(array $criteria, array $orderBy = null)
 * @method IpInfo[]    findAll()
 * @method IpInfo[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class IpInfoRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, IpInfo::class);
    }

    public function findAllIps()
    {
        return $this->createQueryBuilder('i')
            ->select('Min(i.id) as id, i.query')
            ->groupBy('i.query')
            ->orderBy('id', 'DESC')
            ->getQuery()
            ->getResult();
    }

    public function findByIp(string $ip)
    {
        return $this->createQueryBuilder('i')
            ->where('i.query = :ip')
            ->setParameter('ip', $ip)
            ->setMaxResults( 1 )
            ->getQuery()
            ->getOneOrNullResult();
    }
}

<?php

declare(strict_types=1);

namespace App\Controller;

use App\Entity\IpInfo;
use Symfony\Component\Validator\Constraints\Ip;
use Symfony\Contracts\HttpClient\Exception\ClientExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\RedirectionExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\ServerExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\TransportExceptionInterface;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Serializer\SerializerInterface;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpClient\HttpClient;
use Symfony\Component\VarDumper\VarDumper;
use Symfony\Component\Serializer\Normalizer\AbstractNormalizer;

class IpInfoController extends AbstractController
{
    /**
     * @param Request $request
     * @return Response
     * @throws ClientExceptionInterface
     * @throws RedirectionExceptionInterface
     * @throws ServerExceptionInterface
     * @throws TransportExceptionInterface
     * @Route("/", name="main")
     */
    public function indexAction(Request $request, SerializerInterface $serializer)
    {
        $ipInfo = new IpInfo();

        $form = $this->createFormBuilder()
            ->add('ip', TextType::class, [
                'label' => 'Ip:',
                'data' => $request->getClientIp(),
                'constraints' => new Ip()
            ])
            ->add('get', SubmitType::class)
            ->getForm();

        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {

            try {
                $client = HttpClient::create(['http_version' => '2.0']);
                $response = $client->request('GET', 'http://ip-api.com/xml/' . $form->getData()['ip'], [
                    'query' => [
                        'fields' => 59391
                    ]
                ]);
            } catch (TransportExceptionInterface $e) {
                return new Response('failed request');
            }

            $ipInfo = $serializer->deserialize($response->getContent(), IpInfo::class, 'xml', [
                AbstractNormalizer::DEFAULT_CONSTRUCTOR_ARGUMENTS => false,
            ]);

            if ($ipInfo->getStatus() === IpInfo::FAIL) {
                return new Response($response->getContent());
            }

            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($ipInfo);
            $entityManager->flush();

            return $this->render('ipInfo.html.twig', [
                'info' => $ipInfo
            ]);
        }

        return $this->render('index.html.twig', [
            'form' => $form->createView()
        ]);
    }

    /**
     * @Route("/query", name="list_ips")
     */
    public function listRequestIndex()
    {
        $listIps = $this->getDoctrine()
            ->getRepository(IpInfo::class)
            ->findAllIps();

        return $this->render('historyIps.html.twig', [
            'listIps' => $listIps
        ]);
    }

    /**
     * @Route("/query/{ip}", name="ip_info")
     * @param string $ip
     * @return Response
     */
    public function getInfoIp(string $ip)
    {
        $ipInfo = $this->getDoctrine()
            ->getRepository(IpInfo::class)
            ->findByIp($ip);

        return $this->render('ipInfo.html.twig', [
            'info' => $ipInfo
        ]);
    }
}
